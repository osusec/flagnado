from django.urls import path

from . import views

app_name = "challenges"
urlpatterns = [
    path('', views.index, name='index'),

    path('challenges/', views.list_challenges, name='list_challenges'),
    path('challenges/<int:challenge_id>/', views.show_challenge, name='show_challenge'),

    path('exploits/<int:exploit_id>/', views.show_exploit, name='show_exploit'),

    path('servers/', views.list_servers, name='list_servers'),
    path('servers/<int:server_id>', views.show_server, name='show_server')
]