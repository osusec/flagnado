from django.contrib import admin

from .models import Challenge, Exploit, Server

admin.site.register(Challenge)
admin.site.register(Exploit)
admin.site.register(Server)