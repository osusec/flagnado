from django.db import models

import subprocess

class Challenge(models.Model):
    enabled = models.BooleanField(default=True)
    name = models.CharField(max_length=30)
    port = models.IntegerField()

    def __str__(self):
        return self.name

def exploit_path(instance, filename):
    return "{}/exploit_{}.py".format(instance.challenge.name, instance.ranking)

class Exploit(models.Model):
    enabled = models.BooleanField(default=True)
    challenge = models.ForeignKey(Challenge, on_delete=models.CASCADE)
    ranking = models.IntegerField()
    exploit_file = models.FileField(upload_to=exploit_path)
    
    def __str__(self):
        return "Exploit #{}".format(self.ranking)

    # Returns the stdout of a command (as a str)
    # https://github.com/zzzanderw/NFS-Status/blob/master/nfs_status.py#L85
    def get_stdout(cmd):
        try:
            result = subprocess.run(cmd.split(" "), stdout=subprocess.PIPE, timeout=20) # 5 sec timeout
            return result.stdout.decode("utf-8")
        except subprocess.TimeoutExpired:
            return "error"

    def run(self, ip):
        if not self.enabled:
            return "exploit disabled"
        try:
            command = "python2 {} flagnado {} {}".format(self.exploit_file.path, ip, self.challenge.port)
            output = Exploit.get_stdout(command)
            return output.split("FLAGSPLIT")[1].split("FLAGSPLIT")[0].strip()
        except:
            return "error"

class Server(models.Model):
    enabled = models.BooleanField(default=True)
    ip = models.GenericIPAddressField(verbose_name="IP Address", protocol="IPv4", default="0.0.0.0")
    challenges = models.ManyToManyField(Challenge)

    def __str__(self):
        return self.ip