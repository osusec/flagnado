from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse

from .models import Challenge, Exploit, Server
from runner.models import Run, RunnerConfig

#####
# / #
#####

def index(request):
    num_servers = len(Server.objects.filter(enabled=True))
    num_challenges = len(Challenge.objects.filter(enabled=True))
    num_exploits = len(Exploit.objects.filter(enabled=True))

    config = RunnerConfig.load()
    
    runs = Run.objects.all().order_by("-timestamp")[:30]

    context = {
        "num_servers": num_servers,
        "num_challenges": num_challenges,
        "num_exploits": num_exploits,
        "cols": ["challenge", "server", "exploit", "result"],
        "runs": runs,
        "exploiter": config.exploiter_enabled,
        "flagger": config.flagger_enabled 
    }
    return render(request, "challenges/status.html", context)

###############
# /challenges #
###############

def list_challenges(request):
    challs = Challenge.objects.all().order_by("-enabled", "name")

    context = {
        "challenge_list": challs
    }
    return render(request, "challenges/index.html", context)

def show_challenge(request, challenge_id):
    challenge = get_object_or_404(Challenge, pk=challenge_id)

    runs = Run.objects.filter(challenge=challenge).order_by("-timestamp")[:30]

    context = {
        "challenge": challenge,
        "cols": ["server", "exploit", "result"],
        "runs": runs
    }
    return render(request, "challenges/show_challenge.html", context)

#############
# /exploits #
#############

def show_exploit(request, exploit_id):
    exploit = get_object_or_404(Exploit, pk=exploit_id)
    
    with open(exploit.exploit_file.path, "r") as f:
        script = f.read()

    context = {
        "exploit": exploit,
        "script": script
    }

    return render(request, "exploits/show_exploit.html", context)


############
# /servers #
############

def list_servers(request):
    servers = Server.objects.all().order_by("-enabled", "ip")
    return render(request, "servers/index.html", {"servers": servers})

def show_server(request, server_id):
    server = get_object_or_404(Server, pk=server_id)
    
    runs = Run.objects.filter(server=server).order_by("-timestamp")[:30]

    context = {
        "server": server,
        "cols": ["challenge", "exploit", "result"],
        "runs": runs
    }
    return render(request, "servers/show_server.html", context)