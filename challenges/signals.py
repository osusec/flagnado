from django.db.models.signals import post_save
from django.dispatch import receiver

from .models import Challenge, Server

@receiver(post_save)
def post_init_callback(*args, **kwargs):
    instance = kwargs.get("instance")
    
    # add new challenge to all servers
    if isinstance(instance, Challenge):
        for server in Server.objects.all():
            server.challenges.add(instance)
            server.save()