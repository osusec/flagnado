#!/bin/sh

if [ "$#" -ne 1 ]; then
	echo "not enough args" >&2
	exit 1
fi

cd /path/to/flagnado

# load virtualenv
. venv/bin/activate

python manage.py cron $1
