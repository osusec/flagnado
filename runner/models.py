from django.db import models

import requests

from challenges.models import Challenge, Exploit, Server

# https://steelkiwi.com/blog/practical-application-singleton-design-pattern/
class SingletonModel(models.Model):
    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.pk = 1
        super(SingletonModel, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        pass

    @classmethod
    def load(cls):
        obj, created = cls.objects.get_or_create(pk=1)
        return obj

class Flag(models.Model):
    timestamp = models.DateTimeField(auto_now_add=True)
    submitted = models.BooleanField(default=False)
    challenge = models.ForeignKey(Challenge, on_delete=models.DO_NOTHING, blank=True, null=True)
    flag = models.CharField(max_length=50)

    def __str__(self):
        return "{} - {}... ({})".format(self.challenge, self.flag[:10], "submitted" if self.submitted else "not submitted")

    def submit(self):
        try:
            requests.post("http://plspwn.me:443", data={"flag": self.flag})

            self.submitted = True
            self.save()
        except:
            pass

class Run(models.Model):
    timestamp = models.DateTimeField(auto_now_add=True)
    successful = models.BooleanField(default=False)
    flag = models.ForeignKey(Flag, on_delete=models.DO_NOTHING, blank=True, null=True)
    challenge = models.ForeignKey(Challenge, on_delete=models.DO_NOTHING, blank=True, null=True)
    server = models.ForeignKey(Server, on_delete=models.DO_NOTHING, blank=True, null=True)
    exploit = models.ForeignKey(Exploit, on_delete=models.DO_NOTHING, blank=True, null=True)

    def __str__(self):
        return "{} on {} using exploit {} ({})".format(self.challenge, self.server, self.exploit.ranking, "successful" if self.successful
         else "unsuccessful")

class RunnerConfig(SingletonModel):
    exploiter_enabled = models.BooleanField(default=True)
    exploiter_interval = models.IntegerField(default=300)

    flagger_enabled = models.BooleanField(default=True)
    flagger_interval = models.IntegerField(default=30)

    def __str__(self):
        return "Config"