from django.core.management.base import BaseCommand, CommandError

from runner import flagger, exploiter

class Command(BaseCommand):
    help = 'Run a cron task'

    def add_arguments(self, parser):
        parser.add_argument('task', type=str)

    def handle(self, *args, **options):
        task = options["task"]

        if task == "exploiter":
            self.stdout.write(self.style.SUCCESS("Running Exploiter"))
            exploiter.run()
        # elif task == "flagger":
        #     self.stdout.write(self.style.SUCCESS("Running Flagger"))
        #     flagger.run()
        else:
            self.stderr.write(self.style.ERROR("Bad command"))