from .models import Run, Flag, RunnerConfig

import requests

def run():
    config = RunnerConfig.load()

    if not config.flagger_enabled:
        return

    flags = Flag.objects.filter(submitted=False).order_by("-timestamp")

    for flag in flags:
        try:
            submit_api(flag.flag, flag.challenge)
            flag.submitted = True
            flag.save()
        except:
            continue
    
#    Flag.save()

def submit_web(flag, challenge):
    pass

def submit_api(flag, challenge):
    requests.post("http://plspwn.me:443", data={"flag": flag})