from django.contrib import admin

from .models import Flag, Run, RunnerConfig

admin.site.register(Flag)
admin.site.register(Run)
admin.site.register(RunnerConfig)